# Add your targets (i.e. the name of your output program) here
BINARIES = pixy_driver pixy_example pixy_example_threaded comms_driver comms_test px4flow_driver px4flow_example 

BINARIES := $(addprefix bin/,$(BINARIES))
DEPS = src/util.o
BBBLD = bbblib/bbb_init.o bbblib/bbb_pwm.o bbblib/bbb_i2c.o
LCMTYPES = cfg_data_frequency_t.o cfg_usb_serial_num_t.o imu_data_t.o led_t.o pixy_t.o \
		 telemetry_t.o cfg_uart_baud_t.o channels_t.o kill_t.o pixy_frame_t.o rpms_t.o \
		 px4flow_basic_t.o px4flow_integral_t.o px4flow_t.o
LCMTYPES := $(addprefix lcmtypes/,$(LCMTYPES))
CC = gcc
CFLAGS = -g -Wall -std=gnu99 `pkg-config --cflags lcm`
LDFLAGS = `pkg-config --libs lcm`


.PHONY: all clean io lcmtypes bbblib

all: lcmtypes io bbblib $(BINARIES)

lcmtypes:
	@$(MAKE) -C lcmtypes

bbblib:
	@$(MAKE) -C bbblib

clean:
	@$(MAKE) -C lcmtypes clean
	@$(MAKE) -C bbblib clean
	rm -f *~ *.o bin/* blocks/io/*.o src/*.o pixy/*.o PX4Flow/*.o blocks/*.o

%.o: %.c
	$(CC) $(CFLAGS) -c $^ -o $@

lcmtypes/%.o: lcmtypes/%.c
	$(CC) $(CFLAGS) -c $^ -o $@

src/%.o: src/%.c
	$(CC) $(CFLAGS) -c $^ -I. -o $@

blocks/io/%.o: io/%.c
	$(CC) $(CFLAGS) -c $^ -I. -o $@

bin/pixy_driver: pixy/pixy_driver.o src/util.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o
	$(CC) -o $@ $^ $(LDFLAGS)

bin/pixy_example: pixy/pixy_example.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o
	$(CC) -o $@ $^ $(LDFLAGS)

bin/pixy_example_threaded: pixy/pixy_example_threaded.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o
	$(CC) -o $@ $^ $(LDFLAGS)

bin/comms_driver: blocks/comms_driver.o blocks/io/comms.o blocks/io/serial.o blocks/io/circular.o $(LCMTYPES) 
	$(CC) -o $@ $^ -Iio $(LDFLAGS)

bin/comms_test: blocks/comms_test.o $(DEPS)
	$(CC) -o $@ $^ $(LDFLAGS) $(BBBLD) $(LCMTYPES) -lm

bin/px4flow_driver: PX4Flow/PX4Flow_driver.o PX4Flow/PX4Flow.o
	$(CC) -o $@ $^ $(DEPS) $(LDFLAGS) $(LCMTYPES) $(BBBLD) -lm

bin/px4flow_example: PX4Flow/PX4Flow_example.o src/filter_util.o
	$(CC) -o $@ $^ $(DEPS) $(LDFLAGS) $(LCMTYPES) -lm

# Add build commands for your targets here
