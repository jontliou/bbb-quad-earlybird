#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <lcm/lcm.h>
#include <pthread.h>

#define EXTERN

#include "PX4Flow_example.h"
#include "../lcmtypes/px4flow_t.h"
#include "../lcmtypes/px4flow_basic_t.h"
#include "../lcmtypes/px4flow_integral_t.h"
#include "../include/filter_util.h"

pthread_mutex_t handler_mutex;

// Raw values lists, need to store enough values to use in median filter
double vel_x[NUM_SAMPLES_MED], vel_y[NUM_SAMPLES_MED], gnd_dist[NUM_SAMPLES_MED];
// Median filtered values lists, need to store enough values to use in averaging filter
double vel_x_med[NUM_SAMPLES_AVG], vel_y_med[NUM_SAMPLES_AVG], gnd_dist_med[NUM_SAMPLES_AVG];
// Averaging filtered values lists, could actually be scalars
double vel_x_avg[NUM_SAMPLES_AVG], vel_y_avg[NUM_SAMPLES_AVG], gnd_dist_avg[NUM_SAMPLES_AVG];

void px4flow_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const px4flow_t *msg, void *userdata){

    FILE *raw, *filtered;
    raw = fopen("raw.txt","a");
    filtered = fopen("filtered.txt","a");

//    fprintf(raw, "%d,%d,%d", msg->flow_comp_m_x, msg->flow_comp_m_y, msg->ground_distance);
    fprintf(raw,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n", msg->flow_comp_m_x, msg->flow_comp_m_y, msg->qual, msg->ground_distance, msg->pixel_flow_x_integral, msg->pixel_flow_y_integral, msg->gyro_x_rate_integral, msg->gyro_y_rate_integral, msg->gyro_z_rate_integral, msg->integral_ground_distance, msg->quality);
    //printf("Raw x vel: %d, \t\t Raw y vel: %d, \t\t Raw ground dist: %d\n", msg->flow_comp_m_x, msg->flow_comp_m_y, msg->ground_distance);

    // Filter /////////////////////////////////////////////////////////////////////////
    // add arguments to raw values lists
    addArr(vel_x, msg->flow_comp_m_x, NUM_SAMPLES_MED);
    addArr(vel_y, msg->flow_comp_m_y, NUM_SAMPLES_MED);
    addArr(gnd_dist, msg->ground_distance, NUM_SAMPLES_MED);

    /* in recognition that ground distance values less than or equal to zero are invalid,
       they will be replaced with the previous sample value (on the ground, mounted, ground
       distance reads 300 mm) */
    if(gnd_dist[NUM_SAMPLES_MED-1] < 10){
      gnd_dist[NUM_SAMPLES_MED-1] = gnd_dist[NUM_SAMPLES_MED-2];
    }

    // Median filter and add values to median filters lists
    addArr(vel_x_med, median(vel_x, NUM_SAMPLES_MED), NUM_SAMPLES_AVG);
    addArr(vel_y_med, median(vel_y, NUM_SAMPLES_MED), NUM_SAMPLES_AVG);
    addArr(gnd_dist_med, median(gnd_dist, NUM_SAMPLES_MED), NUM_SAMPLES_AVG);

    // Average filter and add values to average filters lists
    addArr(vel_x_avg, average(vel_x_med, NUM_SAMPLES_AVG), NUM_SAMPLES_AVG);
    addArr(vel_y_avg, average(vel_y_med, NUM_SAMPLES_AVG), NUM_SAMPLES_AVG);
    addArr(gnd_dist_avg, average(gnd_dist_med, NUM_SAMPLES_AVG), NUM_SAMPLES_AVG);
    
    fprintf(filtered,"%.2f,%.2f,%.2f\n", vel_x_avg[NUM_SAMPLES_AVG-1], vel_y_avg[NUM_SAMPLES_AVG-1], gnd_dist_avg[NUM_SAMPLES_AVG-1]);
    //printf("Filtered: x vel: %.2f, \t\t y vel: %.2f, \t\t gnd dist: %.2f\n", vel_x_avg[NUM_SAMPLES_AVG-1], vel_y_avg[NUM_SAMPLES_AVG-1], gnd_dist_avg[NUM_SAMPLES_AVG-1]);

    fclose(raw);
    fclose(filtered);
}

int main(int argc, char* argv[]){

  //initialize();
  memset(vel_x,0,sizeof(vel_x));
  memset(vel_y,0,sizeof(vel_y));
  memset(gnd_dist,0,sizeof(gnd_dist));
  memset(vel_x_med,0,sizeof(vel_x_med));
  memset(vel_y_med,0,sizeof(vel_y_med));
  memset(gnd_dist_med,0,sizeof(gnd_dist_med));
  memset(vel_x_avg,0,sizeof(vel_x_avg));
  memset(vel_y_avg,0,sizeof(vel_y_avg));
  memset(gnd_dist_avg,0,sizeof(gnd_dist_avg));

  lcm_t* lcm = lcm_create(NULL);

  px4flow_t_subscribe(lcm, "PX4FLOW", px4flow_handler, NULL);

  while(1){
    lcm_handle(lcm);
  }

  lcm_destroy(lcm);
}
