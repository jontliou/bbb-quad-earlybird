#ifndef _EXAMPLE_H
#define _EXAMPLE_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>

#include "../include/filter_util.h"

#define NUM_SAMPLES_MED 5
#define NUM_SAMPLES_AVG 20

#endif


