# BEAGLEBONE BLACK QUADROTOR SOFTWARE #

This is the SkySpecs Quadrotor Beaglebone Black Software Repository for Robotics and Aerospace Engineering projects.


### CONTENTS (bbb-quad) ###

* bbblib:  Drivers for GPIO, ADC, I2C, PWM interfaces with test programs
* blocks:  Software to interface with the BLOCKS PCBs that read data from pilot R/C & write to DJI Naza
* lcmtypes:  Software to declare data types for LCM (Lightweight Communication Marshalling)
* pixy:  Software to interface with Pixy camera (CMUCAM5)
* PX4Flow:  Software to interface with the PX4 optic flow sensor

### Who do I talk to? ###

Peter Gaskell (pgaskell@umich.edu), Ella Atkins (ematkins@umich.edu)